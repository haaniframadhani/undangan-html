

  // $('#cover').show();
  // $('#mempelai').hide();
  // $('#akad').hide();
  // $('#gallery').hide();
  // $('#ucapan').hide();

  // $('#cover-button').on('click', function(e) {
  //   e.preventDefault();
  //   $('#cover').show();
  //   $('#mempelai').hide();
  //   $('#akad').hide();
  //   $('#gallery').hide();
  //   $('#ucapan').hide();
  // });

  // $('#mempelai-button').on('click', function(e) {
  //   e.preventDefault();
  //   $('#cover').hide();
  //   $('#mempelai').show();
  //   $('#akad').hide();
  //   $('#gallery').hide();
  //   $('#ucapan').hide();
  // });

  // $('#akad-button').on('click', function(e) {
  //   e.preventDefault();
  //   $('#cover').hide();
  //   $('#mempelai').hide();
  //   $('#akad').show();
  //   $('#gallery').hide();
  //   $('#ucapan').hide();
  // });

  // $('#gallery-button').on('click', function(e) {
  //   e.preventDefault();
  //   $('#cover').hide();
  //   $('#mempelai').hide();
  //   $('#akad').hide();
  //   $('#gallery').show();
  //   $('#ucapan').hide();
  // });

  // $('#ucapan-button').on('click', function(e) {
  //   e.preventDefault();
  //   $('#cover').hide();
  //   $('#mempelai').hide();
  //   $('#akad').hide();
  //   $('#gallery').hide();
  //   $('#ucapan').show();
  // });


  $('#form-hadir').hide();

  $('#hadir').on('click', function(e) {
    e.preventDefault();
    $('#form-hadir').slideDown();
  });

  $('#batal').on('click', function(e) {
    e.preventDefault();
    $('#form-hadir').slideUp();
  });

  $('#hadiah').hide();
  $('#hadiah-button').on('click', function(e) {
    e.preventDefault();
    $('#hadiah').slideDown();
  });

  $('.give').on('click', function(){ 
    $(this).addClass('active');
  });

  $('.nominal').hide();
  $('.lainnya').on('click', function(e) {
    e.preventDefault();
    $('.nominal').slideDown();
    $('.nominal').val(0);
    $(".lainnya").addClass("active");
    $(".rp50").removeClass("active");
    $(".rp100").removeClass("active");
    $(".rp300").removeClass("active");
    $(".rp500").removeClass("active");
  });

  $('.rp50').on('click', function(e) {
    $('.nominal').val(50000);
    $('.nominal').slideUp();
    $(".lainnya").removeClass("active");
    $(".rp50").addClass("active");
    $(".rp100").removeClass("active");
    $(".rp300").removeClass("active");
    $(".rp500").removeClass("active");
  });

  $('.rp100').on('click', function(e) {
    $('.nominal').val(100000);
    $('.nominal').slideUp();
    $(".lainnya").removeClass("active");
    $(".rp50").removeClass("active");
    $(".rp100").addClass("active");
    $(".rp300").removeClass("active");
    $(".rp500").removeClass("active");
  });

  $('.rp300').on('click', function(e) {
    $('.nominal').val(300000);
    $('.nominal').slideUp();
    $(".lainnya").removeClass("active");
    $(".rp50").removeClass("active");
    $(".rp100").removeClass("active");
    $(".rp300").addClass("active");
    $(".rp500").removeClass("active");
  });

  $('.rp500').on('click', function(e) {
    $('.nominal').val(500000);
    $('.nominal').slideUp();
    $(".lainnya").removeClass("active");
    $(".rp50").removeClass("active");
    $(".rp100").removeClass("active");
    $(".rp300").removeClass("active");
    $(".rp500").addClass("active");
  });

  // count down 

  // Set the date we're counting down to
  var countDownDate = new Date("Feb 20, 2022 09:00:00").getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();
      
    // Find the distance between now and the count down date
    var distance = countDownDate - now;
      
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      
    // document.getElementById("timer").innerHTML = days + "d " + hours + "h "
    // + minutes + "m " + seconds + "s ";
      
    // If the count down is over, write some text 
    if (distance < 0) {
      clearInterval(x);
      $("#timer").html('EXPIRED');
    } else {
      // Output the result in an element with id="timer"
      $(".days").html(days);
      $(".hours").html(hours);
      $(".minutes").html(minutes);
      $(".seconds").html(seconds);
    }
  }, 1000);

$(function(){
    var overlay = $('<div id="overlay"></div>');
    overlay.show();

    overlay.appendTo(document.body);
    $('.popup').show();
    $('.close').click(function(){
        $('.popup').hide();
        overlay.appendTo(document.body).remove();
        return false;
    });

    $('.x').click(function(){
        $('.popup').hide();
        overlay.appendTo(document.body).remove();
        return false;
    });
});


$('.buka').on('click', function() {
  $('.cover').remove();
})